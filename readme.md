# E-commerce API

### Features:
    1. User registration
	2. User authentication
	3. Create Product (Admin only)
	4. Retrieve all active products
	5. Retrieve all products (Admin only)
	6. Retrieve single product
	7. Update Product information (Admin only)
	8. Archive Product (Admin only)
	9. Non-admin User checkout (Create Order)
	10. Retrieve User Details
    11. Set user as admin (Admin only)
	12. Retrieve authenticated user’s orders
	13. Retrieve all orders (Admin only)
    
### How to use:
    1. Clone project
    2. Add mongoose connection link in .env
    3. Run server using -- npm run dev
    4. Import ecommerce.postman_collection.json in Postman
    5. Provide necessary Token for the requests
        -User - requires User Token
        -Admin - requires Admin Token
        -All - does not require a Token